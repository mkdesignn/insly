<?php
namespace task2\Controller;

class CalculatorController
{
    /**
     * @var int
     */
    private $carValue;

    /**
     * @var int
     */
    private $tax;

    /**
     * @var int
     */
    private $installment;

    /**
     * @var int
     */
    private $basePrice = 11;

    /**
     * @var int
     */
    private $commission = 17;

    /**
     * @var
     */
    private $clientTime;

    /**
     * @var array
     */
    private $calculation = ['installments'=>[], 'totalBasePrice'=>0,
        'totalCommission'=>0, 'totalTax'=>0, 'totalPrice'=>0];

    /**
     * calculator constructor.
     * @param int $carValue
     * @param int $tax
     * @param int $installment
     * @param $clientTime
     */
    public function __construct($carValue, $tax, $installment, $clientTime)
    {
        $this->carValue = $carValue;
        $this->tax = $tax;
        $this->installment = $installment;
        $this->clientTime = $clientTime;
    }

    public function init()
    {
        return $this->calculate();
    }

    private function calculate()
    {
        for($i = 1; $i <= $this->installment; $i++) {

            if($this->nextMonthIsException($i)){
                $this->basePrice = 13;
            } else {
                $this->basePrice = 11;
            }

            $basePrice = round(($this->basePrice * $this->carValue / 100) / $this->installment, 2);
            $commission = round($basePrice * $this->commission / 100, 2);
            $tax = round($basePrice * $this->tax / 100, 2);

            $this->calculation['installments']['basePrice'][] = $basePrice;
            $this->calculation['installments']['commission'][] = $commission;
            $this->calculation['installments']['tax'][] = $tax;
            $this->calculation['installments']['total'][] = $tax + $basePrice + $commission;


            $this->calculation['totalBasePrice'] += $basePrice;
            $this->calculation['totalCommission'] += $commission;
            $this->calculation['totalTax'] += $tax;
            $this->calculation['totalPrice'] += $tax + $basePrice + $commission;
        }

        return $this->calculation;
    }

    private function nextMonthIsException($month)
    {
        $dayHour = date("l-H", strtotime("+".$month." month", strtotime($this->clientTime)));
        $dayHour = explode("-", $dayHour);
        if($dayHour[0] === 'Friday' && ($dayHour[1] >= '15' && $dayHour[1] <= '20')){
            return true;
        }

        return false;
    }
}