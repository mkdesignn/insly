<?php

use task2\Controller\CalculatorController;

require_once dirname(__DIR__) . '/vendor/autoload.php';
?>
<html>
    <header>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </header>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <br>
                    <br>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">

                </div>
                <div class="col-md-8 col-lg-8 col-sm-8">
                    <form method="post" action="index.php">
                        <h4>
                            Calculator
                        </h4>
                        <hr>
                        <div class="form-group">
                            <label for="base_value">Enter the value of the car (100, 100000 Eur)</label>
                            <input type="number" value="<?php echo isset($_POST['car_value']) ? $_POST['car_value'] : '100' ?>" class="form-control" min="100" max="100000" name="car_value" id="base_value" placeholder="base value">
                        </div>
                        <div class="form-group">
                            <label for="tax">Tax percentage (0, 100)</label>
                            <input type="number" class="form-control" value="<?php echo isset($_POST['tax']) ? $_POST['tax'] : '0' ?>" min="0" max="100" id="tax" name="tax" placeholder="tax percentage" >
                        </div>
                        <div class="form-group">
                            <label for="tax">Number of installments</label>
                            <input type="number" class="form-control" value="<?php echo isset($_POST['installment']) ? $_POST['installment'] : '1' ?>" min="1" max="12" id="tax" name="installment" placeholder="installment" >
                        </div>
                        <input type="hidden" name="client_time" id="client_time"/>
                        <input type="submit" name="submit" value="calculate" class="btn btn-info"/>
                    </form>
                    <?php
                    $result = [];
                    if(isset($_POST['car_value']) && isset($_POST['tax']) && isset($_POST['installment']) && isset($_POST['client_time']) && isset($_POST['submit'])){
                        $carValue = $_POST['car_value']; //10000;
                        $tax = $_POST['tax']; //10;
                        $installment =  $_POST['installment']; //2;
                        $clientTime = $_POST['client_time']; // "2020/01/07 16:00:00";
                        $calculator = new CalculatorController($carValue, $tax, $installment, $clientTime);
                        $result = $calculator->init();
                    }
                    if(count($result) > 0):
                    ?>
                        <table class="table" cellpadding="10">
                            <thead>
                                <th colspan="2">
                                    Policy
                                </th>
                                <?php foreach($result['installments']['tax'] as $key => $tax): ?>
                                    <th>
                                        <?php echo $key + 1 ?> Installment
                                    </th>
                                <?php endforeach; ?>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        Base premium (11%)
                                    </td>
                                    <td><?php echo $result['totalBasePrice']; ?></td>
                                    <?php foreach($result['installments']['basePrice'] as $basePrice){ ?>
                                        <td>
                                            <?php echo $basePrice; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td>
                                        Commission (17%)
                                    </td>
                                    <td><?php echo $result['totalCommission']; ?></td>
                                    <?php foreach($result['installments']['commission'] as $commission){ ?>
                                        <td>
                                            <?php echo $commission; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td>
                                        Tax (10%)
                                    </td>
                                    <td><?php echo $result['totalTax']; ?></td>
                                    <?php foreach($result['installments']['tax'] as $tax){ ?>
                                        <td>
                                            <?php echo $tax; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td>
                                        Total cost
                                    </td>
                                    <td><?php echo $result['totalPrice']; ?></td>
                                    <?php foreach($result['installments']['total'] as $total){ ?>
                                        <td>
                                            <?php echo $total; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <script>
        let date = new Date();
        document.getElementById("client_time").value = date.getFullYear()+"/"+(date.getMonth() + 1)+"/"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
    </script>
    </body>
</html>