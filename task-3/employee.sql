set foreign_key_checks=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `family` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) DEFAULT NULL,
    ssn varchar (11) DEFAULT null,
    birth_date date DEFAULT NULL,
    email VARCHAR(255) DEFAULT NULL,
    phone varchar(100) DEFAULT null,
    address varchar(255) DEFAULT null,
    created_by int(10) unsigned DEFAULT NULL,
    updated_by int(10) unsigned DEFAULT NULL,
    created_at timestamp NULL DEFAULT current_timestamp,
    updated_at timestamp NULL DEFAULT NULL,
    PRIMARY KEY(id),
    CONSTRAINT created_by FOREIGN KEY (created_by) REFERENCES users (id),
    CONSTRAINT updated_by FOREIGN KEY (updated_by) REFERENCES users (id),
    UNIQUE KEY `employee_ssn_unique` (`ssn`),
    UNIQUE KEY `employee_employee_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for employee_meta
-- ----------------------------
DROP TABLE IF EXISTS employee_meta;
CREATE TABLE employee_meta (
  id int(11) NOT NULL AUTO_INCREMENT,
  employee_id int(10) unsigned DEFAULT NULL,
  introduction varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  prev_work_experience varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  education_information varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  lang enum('en','sp','fr') COLLATE utf8mb4_unicode_ci DEFAULT 'en',
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT employee_id FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;