<?php
namespace tests\unit;

use task2\Controller\CalculatorController;

class CalculatorTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @test
     * @see CalculatorController::init()
     */
    public function init_should_return_correct_calculation_while_passing_specific_parameters()
    {
        //$carValue, $tax, $installment, $clientTime
        $calculator = new CalculatorController(1000, 11, 2, "2019/01/01 12:00:00");
        $result = $calculator->init();

        $this->assertEquals([
            'installments'=>[
                'basePrice'=>[55, 55],
                'commission'=>[9.35, 9.35],
                'tax'=>[6.05, 6.05],
                'total'=>[70.4, 70.4]
            ],
            'totalBasePrice'=>110.0,
            'totalCommission'=>18.7,
            'totalTax'=>12.1,
            'totalPrice'=>140.8
        ], $result);
    }


    /**
     * @test
     * @see CalculatorController::init()
     */
    public function init_should_return_correct_calculation_even_if_the_installment_was_located_in_an_exceptional_time()
    {
        // First month will be 2020/02/07 which it's friday and also its at 16:00
        // Second month will be 2020/03/07 which it's saturday
        $calculator = new CalculatorController(1000, 11, 2, "2020/01/07 16:00:00");
        $result = $calculator->init();

        $this->assertEquals([
            'installments'=>[
                'basePrice'=>[65, 55],
                'commission'=>[11.05, 9.35],
                'tax'=>[7.15, 6.05],
                'total'=>[83.2, 70.4]
            ],
            'totalBasePrice'=>120.0,
            'totalCommission'=>20.4,
            'totalTax'=>13.2,
            'totalPrice'=>153.6
        ], $result);
    }
}